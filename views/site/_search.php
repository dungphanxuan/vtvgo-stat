<?php

/* @var $this yii\web\View */

?>
<div class="stat-search">

    <div class="row">
        <div class="col-lg-12">
            <form class="form-inline" method="get">
                <div class="form-group">
                    <label for="exampleInputName2">Ngày tháng</label>
                    <input type="text" class="form-control" id="exampleInputName2">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail2">Tên chương trình</label>
                    <input type="text" class="form-control" id="exampleInputEmail2">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail2">Kênh</label>
                    <input type="text" class="form-control" id="exampleInputEmail2">
                </div>
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
            </form>
            <br>
        </div>
    </div>
</div>
