<?php

/* @var $this yii\web\View */

?>
<div class="stat-search">

    <div class="row">
        <div class="col-lg-12">
            <form class="form-inline" method="get">
                <div class="form-group">
                    <label for="exampleInputName2">Ngày tháng</label>
                    <?php echo trntv\yii\datetime\DateTimeWidget::widget([
                        'name'              => 'date',
                        'value' => \Yii::$app->request->get('date', date("Y-m-d")),
                        'phpDatetimeFormat' => 'yyyy-MM-dd',
                        'clientOptions' => [
                            'minDate' => new \yii\web\JsExpression('new Date("2018-01-01")'),
                            'allowInputToggle' => true,
                            'sideBySide' => true,
                            'locale' => 'vi-VN',
                            'widgetPositioning' => [
                                'horizontal' => 'auto',
                                'vertical' => 'auto'
                            ]
                        ]
                    ]); ?>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail2">Tên chương trình</label>
                    <input type="text" class="form-control" id="exampleInputEmail2">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail2">Kênh</label>
                    <input type="text" class="form-control" id="exampleInputEmail2">
                </div>
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
            </form>
            <br>
        </div>
    </div>
</div>
