<?php
/* @var $this yii\web\View */
$this->title = 'Thống kê truy cập';
?>
    <h1><?php echo $this->title ?></h1>
<?php echo $this->render('_search', []); ?>

<?php
/** @var \yii\data\ArrayDataProvider $provider */
echo \yii\grid\GridView::widget([
    'dataProvider' => $provider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        // Simple columns defined by the data contained in $dataProvider.
        // Data from the model's column will be used.
        'id',
        [
            'attribute' => 'epg_id',
            'format' => 'raw',
            'label' => 'ID Chương trình'
        ],
        'vod_title',
        [
            'attribute' => 'epg_title',
            'format' => 'raw',
            'label' => 'Chương trình'
        ],
        [
            'attribute' => 'vtv_id',
            'format' => 'raw',
            'label' => 'Kênh'
        ],
        [
            'attribute' => 'view_count',
            'format' => 'raw',
            'label' => 'Lượt xem'
        ],
    ],
]);