<?php
/* @var $this yii\web\View */
/* @var $provider \yii\data\ArrayDataProvider */
?>
    <h1>test/index</h1>

    <p>
        You may change the content of this page by modifying
        the file <code><?= __FILE__; ?></code>.
    </p>
<?php
echo \yii\grid\GridView::widget([
    'dataProvider' => $provider,
    'layout' => "{summary}\n{pager}\n{items}\n{pager}",
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        // Simple columns defined by the data contained in $dataProvider.
        // Data from the model's column will be used.
        'id',
        'vod_title',
        'epg_detail',
        'vtv_id'
    ],
]);